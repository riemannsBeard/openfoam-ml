# Machine Learning in OpenFOAM  

## Surface Approximation

Neural Networks for approximation of implicit surfaces on unstructured meshes.

## Deep Reinforcement Learning 

Deep Reinforcement Learning algorithms for solution control of coupled PDEs. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
* OpenFOAM: tested with v2006) 
* libtorch: C++ API for pytorch, tested with 1.5.0-2 
```

### Installing

Install libtorch PyTorch C++ API either using a package manager, or download the archive from the [PyTorch website](https://pytorch.org/get-started/locally/) and extract the archive in the `openfoam-ml` folder. 

For example, get the latest CPU distribution of libtorch, 

```
    openfoam-ml> wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-1.7.1%2Bcpu.zip 
    openfoam-ml> unzip libtorch-cxx11-abi-shared-with-deps-1.7.1+cpu.zip
```

Configure variables used to include and link PyTorch files and libraries, 

```
    openfoam-ml> source setup-torch.sh
```

If not already set up, set up the OpenFOAM Environment 
```
    openfoam-ml> source /path/to/openfoam/etc/bashrc
```

Build `openfoam-ml` by calling 

```
?> ./Allwmake
```

## Running the tests

**TODO**: Explain how to run the automated tests for this system

### Break down into end to end tests

#### Surface approximation 

`unit_box_domain` test

```
?> blockMesh 
?> aiFoamLearnSurface
```

Fields with `nn` in their name are approximated by Neural Networks and can be visualized in ParaView. 

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests.

## Authors

* **Tomislav Maric** - *Main Developer* - [Contact](tomislav.maric@gmx.com)

<!--See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.-->

## License

This project is licensed under the GPL 3.0 License. 
