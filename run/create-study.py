#!/usr/bin/python 

from optparse import OptionParser
import sys
from subprocess import call
import os


def set_up_dimensions (dim_1, dim_2): 
    """Renames 0/field.dim_1 to 0/field and removes 0/field.dim_2"""
    files_dim_1 = [os.path.join("0",fname) for fname in os.listdir("0") \
                if os.path.isfile(os.path.join("0", fname)) and \
                fname.endswith(dim_1)] 
    for file_dim_1 in files_dim_1:
        base_name = file_dim_1.rstrip("." + dim_1)
        call(["mv", file_dim_1, base_name])
        call(["rm", "-rf", base_name + "." + dim_2])

usage = """A wrapper for pyFoamRunParameterVariation.py that generates the
directory structure for a parameter study. 

Meshes are not generated and preprocessing is not done. 
Used to prepare for execution on a cluster.

Usage: ./create-study.py -c templateCase -p paramFile -s studyName"""

parser = OptionParser(usage=usage)

parser.add_option("-c", "--case", dest="casedir",
                  help="Template case directory.", 
                  metavar="CASEDIR")

parser.add_option("-p", "--parameter-file", dest="paramfile", 
                  help="PyFoam parameter file used by pyFoamRunParameterVariation.py.", 
                  metavar="PARAMFILE")

parser.add_option("-s", "--study-name", dest="studyname", 
                  help="Name of the parameter study.", 
                  metavar="STUDYNAME")

parser.add_option("-d", "--dimension", dest="dimension", 
                  help="2D or 3D.", 
                  metavar="DIMENSION")

if __name__ == "__main__":

    (options, args) = parser.parse_args()

    if ((options.casedir == None) or  
        (options.paramfile == None) or 
        (options.studyname == None) or
        (options.dimension == None)): 
        print ("Error: case or parameter option not used. Use --help option for more information.") 
        sys.exit(1)

    (options, args) = parser.parse_args()

    # Generate parameter study simulation cases 
    call(["pyFoamRunParameterVariation.py", "--no-execute-solver", "--no-server-process", 
          "--no-mesh-create", "--no-case-setup", "--cloned-case-prefix=%s" % options.studyname, 
          "--every-variant-one-case-execution",
          "--create-database", options.casedir, options.paramfile])

    # Update the simulation cases for 2D or 3D calculation 
    study_dirs = [dir_name for dir_name in os.listdir(os.curdir)  \
                  if os.path.isdir(dir_name) and \
                  (options.studyname in dir_name)]

    root_dir = os.getcwd()
    for study_dir in study_dirs:
        os.chdir(study_dir)
        if (options.dimension == "2D"):
            set_up_dimensions("2D", "3D")
        elif (options.dimension == "3D"):
            set_up_dimensions("3D", "2D")
        os.chdir(root_dir)
