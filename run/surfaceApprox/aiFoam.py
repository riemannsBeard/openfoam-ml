import pandas as pd
import os
import numpy as np

def get_study_dframe(study_pattern, data_file_name):

    data_dirs = [item for item in os.listdir(os.curdir) 
                 if os.path.isfile(os.path.join(item, data_file_name))
                 and study_pattern in item]
    
    data_dirs.sort()
    
    study_dframe = pd.DataFrame()
    
    for item in data_dirs:
        case_dframe = pd.read_csv(os.path.join(item, data_file_name), index_col=[0,1,2])
        study_dframe = pd.concat([study_dframe, case_dframe])
        
    # Dump data for backup
    study_dframe.to_csv("convergence2D_00.csv")
    study_dframe.to_latex("convergence2D_00.tex")
    
    return study_dframe


def second_order_convergent(h_old, h_new, e_old):
    return np.exp(2*np.log(h_new/h_old))*e_old