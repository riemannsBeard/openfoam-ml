#!/bin/bash
if [ "$1" = "sinc" ]; then
        ../create-study.py -s convergence2D_sinc -p unit_box_domain.parameter -c unit_box_domain -d 2D
        ../bulkeval convergence2D_sinc_0000 "blockMesh"
        ../bulkeval convergence2D_sinc_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) 0.5 50' -volFieldName psi_c"
elif [ "$1" = "plane" ]; then
        ../create-study.py -s convergence2D_plane -p unit_box_domain.parameter -c unit_box_domain -d 2D
        ../bulkeval convergence2D_plane_0000 "blockMesh"
        ../bulkeval convergence2D_plane_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) (0 1 0)' -volFieldName psi_c"
elif [ "$1" = "sphere" ]; then
        ../create-study.py -s convergence2D_sphere -p unit_box_domain.parameter -c unit_box_domain -d 2D
        ../bulkeval convergence2D_sphere_0000 "blockMesh"
        ../bulkeval convergence2D_sphere_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) 0.25' -volFieldName psi_c"
else
        echo "Error: name of the surface not provided as first argument." 
        echo "Available surfaces: sinc, plane, sphere."
        exit 1
fi

