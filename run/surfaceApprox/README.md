# Creating the structure of the parameter study 

**Requires PyFoam**: pip install --user PyFoam

Run 

```
?> ../create-study.py -s convergence2D -p unit_box_domain.parameter -c unit_box_domain
```
to create "convergence2D" study case structure. 

Call `../create-study.py --help` for more information.

* `unit_box_domain.parameter` stores parameter vectors with values. 
* parameter vector values are replaced in `*.template` files inside the template case `unit_box_domain`

# Preparing 2D cases input data

```
?> bash ../bulkeval convergence2D_0000 ". ../../resetCase 2D" 
```

# Meshing 2D cases

```
?> bash ../bulkeval convergence2D_000 "blockMesh" 
```

# Initialization 

Initialize sphere centered at (0.5 0.5 0) with radius 0.25 in the 2D cases

```
?> bash ../bulkeval convergence2D_0000 "aiFoamSetField -surfaceType sphere -surfaceParams '(0.5 0.5 0) 0.25' -volFieldName psi_c"
```

# Surface approximation with piecewise-continous Multilayer Perceptron Networks

## Serial approximation 

Train MLP neural networks on surfaces in 2D cases in serial

```
?> bash ../bulkeval convergence2D_0000 "aiFoamLearnSurface"
```

## Parallel surface approximation on the cluster 

Train MLP neural networks on surfaces in 2D cases in serial on the HPC cluster

```
?> bash ../bulkeval convergence2D_0000 "sbatch ../aiFoamLearnSurface.sbatch"
```

# Result analysis

## Local visualization 

```
?> jupyter-lab pcmlp-surface-approximation.ipynb
```

## Visualization on the cluster

TODO

