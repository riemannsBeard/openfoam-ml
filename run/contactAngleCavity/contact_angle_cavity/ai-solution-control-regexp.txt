CpuTimePerTimeStep
{
  expr "CPU time per time step: (%f%)";
  theTitle "CPU time per time step";
  type dynamic;
  idNr 1;
  with steps;
}
