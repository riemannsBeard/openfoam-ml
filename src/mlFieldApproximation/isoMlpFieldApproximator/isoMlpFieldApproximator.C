/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2020 Tomislav Maric, TU Darmstadt
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::AI::isoMlpFieldApproximator

Description

    Base class for MultiLayer Perceptron (MLP) field approximation in OpenFOAM
    using the libtorch C++ API. 

SourceFiles
    isoMlpFieldApproximator.C

\*---------------------------------------------------------------------------*/

#include "isoMlpFieldApproximator.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "torchFunctions.H"
#include "processorFvPatchField.H"

namespace Foam {
namespace AI { 
    
// * * * * * * * * * * * * * * Static Members * * * * * * * * * * * * * //

defineTypeNameAndDebug(isoMlpFieldApproximator, false);
addToRunTimeSelectionTable(mlpFieldApproximator, isoMlpFieldApproximator, Name);

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void isoMlpFieldApproximator::setTrainingData(const volScalarField& vf)
{
    const auto& mesh = vf.mesh();
    trainingCellLabels_.clear();
    
    const auto& nei = mesh.neighbour();
    const auto& own = mesh.owner();
    
    // Narrow band boolean marker list.
    boolList narrowBand(vf.size(), false);
    
    // For all faces 
    forAll(own, faceI)
    {
        // If the owner / neighbor line segment is intersected by the
        // implicit surface. 
        if (((vf[nei[faceI]] > 0) && (vf[own[faceI]] <= 0)) || 
            ((vf[nei[faceI]] <= 0) && (vf[own[faceI]] >= 0))) 
        {
            // Add both cells to the set of training cells.
            trainingCellLabels_.insert(nei[faceI]);
            trainingCellLabels_.insert(own[faceI]);
        }
    }
    
    // Mark the training cell labels in the narrow band.
    for(const auto& cellI : trainingCellLabels_)
        narrowBand[cellI] = true;
    
    // Make sure the additional layer of positive cells is added
    // to the narrow band, this makes sure the narrow band is three
    // cells thick in the direction of the surface normal. TM. 
    forAll(own, faceI)
    {
        // If centers of both adjacent cells are positive
        if ((vf[nei[faceI]] > 0) && (vf[own[faceI]] > 0))
        {
            // If the neighbor cell is in the narrow band
            if (narrowBand[nei[faceI]])
            {
                // add the owner to the narrow band as well. 
                trainingCellLabels_.insert(own [faceI]);
            }
            // If the face-owner cell is in the narrow band 
            if (narrowBand[own[faceI]])
                // add the face-neighbor to the narrow band.
                trainingCellLabels_.insert(nei[faceI]);
        }
    }

    // TODO: Parallel implementation. test first if the effort makes any sense.
    //       Approximate piecewise-continuously with each MPI process having an
    //       own network without overlap. Report error. If this is lacking, then
    //       implement parallel communication for the halo layer. TM.
    //const auto& vfBoundaryField = vf.boundaryField();
    //const auto& meshBoundary = mesh.boundary(); 
    //forAll(vfBoundaryField, patchI)
    //{
        //const auto& vfPatch = meshBoundary[patchI];
        //if(isA<processorFvPatch>(vfPatch))
        //{
            //const auto& vfPatchFld = vfBoundaryField[patchI];
            //const auto vfNeiPatchFld = vfPatchFld.patchNeighbourField();
            
            //forAll(vfPatchFld, faceI)
            //{
                //if (((vf[nei[faceI]]) > 0 && (vf[own[faceI]]) <= 0) || 
                    //((vf[nei[faceI]]) <= 0 && (vf[own[faceI]]) >= 0)) 
                //{
                    // TODO: Insert bb
                //}
            //}
        //}
    //}

    // Data transfer OpenFOAM -> libtorch. 
    // - Initialize cell center and respective vf tensors. 
    points_ = torch::zeros({static_cast<long>(trainingCellLabels_.size()),3});
    values_ = torch::zeros({static_cast<long>(trainingCellLabels_.size()),1});

    // - Assign cell center and vf values to tensors.
    const auto& cell_centers = mesh.C();
    std::size_t dataI = 0; 
    for(const auto cellI : trainingCellLabels_)
    {
        assign(points_[dataI], cell_centers[cellI]);
        values_[dataI] = vf[cellI];
        ++dataI; 
    }
}

void isoMlpFieldApproximator::evaluate(volScalarField& sigDist)
{
    const auto& mesh = sigDist.mesh(); 
    const auto& cell_centers = mesh.C(); 

    torch::Tensor input_point = torch::zeros(3);
    for(const auto cellI : trainingCellLabels_)
    {
        assign(input_point, cell_centers[cellI]);
        sigDist[cellI] = nn_->forward(input_point).item<double>();
    }
    sigDist.correctBoundaryConditions();
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace AI
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
