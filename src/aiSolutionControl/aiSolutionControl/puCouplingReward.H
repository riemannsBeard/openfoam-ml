/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2020 Tomislav Maric, TU Darmstadt
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::puCouplingReward

Description
    Reward calculation for the AI solution control algorithm used to  
    control pressure-velocity coupling algorithms.

\*---------------------------------------------------------------------------*/

#ifndef puCouplingReward_H
#define puCouplingReward_H

#include <vector>
#include "scalar.H" 
#include "typeInfo.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam { 

/*---------------------------------------------------------------------------*\
                        Class puCouplingReward Declaration
\*---------------------------------------------------------------------------*/

class statistics
{
    label  n_= 0;
    scalar mean0_ = 0;
    scalar mean_ = 0;
    scalar M2n_ = 0;
    scalar stddev_ = 0;
    scalar min_ = GREAT;
    scalar max_ = -GREAT;

    public: 

        void update(double xn)
        {
            //Statistics used for normalizing the reward parameters 
            //are based on the Welford's online algorithm 
            //https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance

            ++n_; 

            if (n_ == 1)
                mean_ = xn;

            // Max 
            if (xn > max_)
                max_ = xn; 

            // Min 
            if (xn < min_)
                min_ = xn;

            // Mean 
            mean0_ = mean_;
            mean_ = mean0_ + (1.0 / n_)*(xn - mean0_);
            
            // Standard deviation.
            M2n_ = M2n_ + (xn - mean0_)*(xn - mean_); 
            stddev_ = Foam::sqrt(M2n_ / n_);
        }

        scalar max() const
        {
            return max_; 
        }

        scalar min() const
        {
            return min_;
        }

        scalar mean() const
        {
            return mean_;
        }

        scalar stddev() const
        {
            return stddev_;
        }

        label n() const
        {
            return n_; 
        }
};

class puCouplingReward 
{
    scalar rho_ = 0;
    statistics rhoStats_; 
    scalar rhoScaled_ = 0;

    scalar tau_ = 0; 
    statistics tauStats_; 
    statistics tauMeanStats_;
    scalar tauScaled_ = 0;

    scalar reward_ = 0;

public: 

    // Static Members

    TypeName("puCouplingReward");

    // Destructor
    // TypeName virtual member function requires a virtual destructor. 
    virtual ~puCouplingReward() = default;

    // Member Functions
    
    void updateTau(const scalar& tau);

    void updateRho(const scalar& rho);

    void calcReward();

    scalar reward() const
    {
        return reward_;
    }

    scalar rho() const
    {
        return rho_;
    }

    scalar rhoMean() const
    {
        return rhoStats_.mean();
    }

    scalar rhoStddev() const 
    {
        return rhoStats_.stddev();
    }

    scalar rhoScaled() const
    {
        return rhoScaled_; 
    }

    scalar tau() const
    {
        return tau_;
    }

    scalar tauMean()
    {
        return tauStats_.mean();
    }

    scalar tauStddev() const 
    {
        return tauStats_.stddev();
    }

    scalar tauScaled() const
    {
        return tauScaled_;
    }

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
