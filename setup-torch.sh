#!/usr/bin/env bash

# Downloaded torch C++ API extracted to openfoam-ml/libtorch

export OF_TORCH=$(pwd)/libtorch/include/
export OF_TORCH_INCLUDE=$OF_TORCH/torch/csrc/api/include
export OF_TORCH_LIB=$(pwd)/libtorch/lib

#  Local Arxh Linux system 

#export OF_TORCH=/usr/include/
#export OF_TORCH_INCLUDE=$OF_TORCH/torch/csrc/api/include/
#export OF_TORCH_LIB=/usr/lib
